// -*- C++ -*-
//
// This file is part of YODA -- Yet more Objects for Data Analysis
// Copyright (C) 2008-2025 The YODA collaboration (see AUTHORS for details)
//
#ifndef YODA_READERYODA_H
#define YODA_READERYODA_H

#include "YODA/Reader.h"

namespace YODA {


  /// Persistency reader from YODA text data format.
  class ReaderYODA : public Reader {
  public:

    /// Singleton creation function
    static Reader& create();

    /// Disable copy and move constructors
    ReaderYODA(const ReaderYODA&) = delete;
    ReaderYODA(ReaderYODA&&) = delete;

    /// Disable copy and move assignments
    void operator=(const ReaderYODA&) = delete;
    void operator=(ReaderYODA&&) = delete;

    void read(std::istream& stream, std::vector<AnalysisObject*>& aos,
                                    const std::string& match = "",
                                    const std::string& unmatch = "");

    // Include definitions of all read methods (all fulfilled by Reader::read(...))
    #include "YODA/ReaderMethods.icc"

  private:

    // Suppress H5 read method
    #ifdef HAVE_HDF5
    void read(const YODA_H5::File&, std::vector<AnalysisObject*>&,
              const std::string& = "", const std::string& = "") { };
    #endif

    /// Private constructor, since it's a singleton.
    ReaderYODA() { }

    /// Helper function to load most common types
    template<typename... Args>
    void registerDefaultTypes();

  };


}

#endif
