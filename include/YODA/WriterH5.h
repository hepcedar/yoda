// -*- C++ -*-
//
// This file is part of YODA -- Yet more Objects for Data Analysis
// Copyright (C) 2008-2023 The YODA collaboration (see AUTHORS for details)
//
#ifndef YODA_WRITERH5_H
#define YODA_WRITERH5_H

#include "YODA/Writer.h"

namespace YODA {


  /// Persistency writer for YODA H5 format.
  class WriterH5 : public Writer {
  public:

    /// Singleton creation function
    static Writer& create();

    // Include definitions of all write methods (all fulfilled by Writer::write(...))
    #include "YODA/WriterMethods.icc"


  protected:

    void writeAOS(YODA_H5::File& file, const std::vector<const AnalysisObject*>& aos);


  private:

    // disable stream-based methods
    //write(std::ostream& stream, const std::vector<const AnalysisObject*>& aos) = delete;

    void writeAO(std::ostream&, const AnalysisObject&) { };

    /// Private since it's a singleton.
    WriterH5() { }

  };


}

#endif
