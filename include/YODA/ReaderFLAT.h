// -*- C++ -*-
//
// This file is part of YODA -- Yet more Objects for Data Analysis
// Copyright (C) 2008-2025 The YODA collaboration (see AUTHORS for details)
//
#ifndef YODA_READERFLAT_H
#define YODA_READERFLAT_H

#include "YODA/Reader.h"

namespace YODA {


  /// Persistency reader from YODA flat text data format.
  class ReaderFLAT : public Reader {
  public:

    /// Singleton creation function
    static Reader& create();

    /// Disable copy and move constructors
    ReaderFLAT(const ReaderFLAT&) = delete;
    ReaderFLAT(ReaderFLAT&&) = delete;

    /// Disable copy and move assignments
    void operator=(const ReaderFLAT&) = delete;
    void operator=(ReaderFLAT&&) = delete;

    void read(std::istream& stream, std::vector<AnalysisObject*>& aos,
                                    const std::string& match = "",
                                    const std::string& unmatch = "");

  private:

    // Suppress H5 read method
    #ifdef HAVE_HDF5
    void read(const YODA_H5::File&, std::vector<AnalysisObject*>&,
              const std::string& = "", const std::string& = "") { };
    #endif

    /// Private constructor, since it's a singleton.
    ReaderFLAT() { }

    /// Helper function to load most common types
    void registerDefaultTypes();

  };

}

#endif
