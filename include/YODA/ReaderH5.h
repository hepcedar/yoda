// -*- C++ -*-
//
// This file is part of YODA -- Yet more Objects for Data Analysis
// Copyright (C) 2008-2023 The YODA collaboration (see AUTHORS for details)
//
#ifndef YODA_READERH5_H
#define YODA_READERH5_H

#include "YODA/Reader.h"

namespace YODA {


  /// Persistency reader from YODA text data format.
  class ReaderH5 : public Reader {
  public:

    /// Singleton creation function
    static Reader& create();

    /// Disable copy and move constructors
    ReaderH5(const ReaderH5&) = delete;
    ReaderH5(ReaderH5&&) = delete;

    /// Disable copy and move assignments
    void operator=(const ReaderH5&) = delete;
    void operator=(ReaderH5&&) = delete;

    void read(const YODA_H5::File& file, std::vector<AnalysisObject*>& aos,
                                          const std::string& match = "",
                                          const std::string& unmatch = "");

    // Include definitions of all read methods (all fulfilled by Reader::read(...))
    #include "YODA/ReaderMethods.icc"

  private:

    // Suppress stream read method
    void read(std::istream&, std::vector<AnalysisObject*>&,
              const std::string& = "", const std::string& = "")  { };

    /// Private constructor, since it's a singleton.
    ReaderH5() { }

    /// Helper function to load most common types
    template<typename... Args>
    void registerDefaultTypes();

  };


}

#endif
