// -*- C++ -*-
//
// This file is part of YODA -- Yet more Objects for Data Analysis
// Copyright (C) 2008-2025 The YODA collaboration (see AUTHORS for details)
//
#include "YODA/Utils/Paths.h"
#include "YODA/Utils/StringUtils.h"
#include "binreloc/binreloc.h"

#include <cstring>

using namespace std;

namespace YODA {


  string getLibPath() {
    BrInitError error;
    br_init_lib(&error);
    char* temp = br_find_lib_dir(DEFAULTLIBDIR);
    const string libdir(temp);
    free(temp);
    return libdir;
  }

  string getDataPath() {
    BrInitError error;
    br_init_lib(&error);
    char* temp = br_find_data_dir(DEFAULTDATADIR);
    const string sharedir(temp);
    free(temp);
    return sharedir + "/YODA";
  }

  vector<string> getYodaDataPath() {
    vector<string> dirs;
    // Use the YODA data path variable if set...
    const char* env = getenv("YODA_DATA_PATH");
    if (env) dirs = Utils::pathsplit(env);
    // ... then, unless the path ends in :: ...
    if (!env || strlen(env) < 2 || string(env).substr(strlen(env)-2) != "::") {
      // ... fall back to the YODA data install path
      dirs.push_back(getDataPath());
    }
    return dirs;
  }


}
