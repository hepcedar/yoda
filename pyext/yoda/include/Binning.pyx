cimport util

class Binning(object):
    def __init__(self, *args):
        if len(args) == 1 and isinstance(args[0], Binning):
            self.axes = list(args[0].axes)
        else:
            self.axes = [ Axis(edges) for edges in args ]

    def config(self):
        return ''.join(a.type() for a in self.axes)

    def width(self, dim, index):
        return self.axes[dim].width(index)

    def max(self, dim, index):
        return self.axes[dim].max(index)

    def min(self, dim, index):
        return self.axes[dim].min(index)

    def mid(self, dim, index):
        return self.axes[dim].mid(index)

    def edge(self, dim, index):
        return self.axes[dim].edge(index)

