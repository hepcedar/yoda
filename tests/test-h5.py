#! /usr/bin/env python

import sys, os

TESTSRCDIR = os.environ.get("YODA_TESTS_SRC", ".")
def testsrcpath(fname):
    return os.path.join(TESTSRCDIR, fname)

import yoda

ypath = testsrcpath("test.yoda")
aos_ref = yoda.read(ypath)

assert len(aos_ref.keys()) > 0
#print(aos_ref.keys())

h5path = ypath + ".h5"
yoda.write(aos_ref, h5path)

aos = yoda.read(h5path)
assert set(aos.keys()) == set(aos_ref.keys())

