#include "YODA/BinnedAxis.h"
#include "YODA/Utils/MetaUtils.h"
#include "YODA/Utils/MathUtils.h"
#include "YODA/Utils/BinEstimators.h"
#include <iostream>

using namespace YODA;

auto testLinEst() {

  LinBinEstimator est1(0, 0, 1);

  LinBinEstimator est2(1, 0, 1);

  return CHECK_TEST_RES(est1.estindex(-1.0) == 0 &&
                        est1.estindex( 0.0) == 0 &&
                        est1.estindex( 0.5) == 0 &&
                        est1.estindex( 0.5) == 0 &&
                        est1.estindex(10.0) == 0 &&
                        est2.estindex(-1.0) == 0 &&
                        est2.estindex( 0.0) == 1 &&
                        est2.estindex( 0.5) == 1 &&
                        est2.estindex( 1.0) == 2 &&
                        est2.estindex(10.0) == 2);
}


auto testLogEst() {
  LogBinEstimator est1(0, 0.1, 10);

  LogBinEstimator est2(1, 0.1, 10);

  return CHECK_TEST_RES(est1.estindex( -1.) == 0 &&
                        est1.estindex(  0.) == 0 &&
                        est1.estindex(  1.) == 0 &&
                        est1.estindex(  5.) == 0 &&
                        est1.estindex( 10.) == 0 &&
                        est1.estindex(100.) == 0 &&
                        est2.estindex( -1.) == 0 &&
                        est2.estindex(  0.) == 0 &&
                        est2.estindex(  1.) == 1 &&
                        est2.estindex(  5.) == 1 &&
                        est2.estindex( 10.) == 2 &&
                        est2.estindex(100.) == 2);
}


int main() {
  int rtn = EXIT_SUCCESS;

  rtn = testLinEst() + testLogEst();

  return rtn;
}
