// #include "YODA/Binning.h"
// #include "YODA/BinnedAxis.h"
// #include "YODA/Utils/MathUtils.h"
// #include "YODA/Utils/Formatting.h"
// using namespace YODA;
// using namespace std;

int main() {}

//   int rtn = EXIT_SUCCESS;

  #define TESTBS(bs, x, iref) \
    MSG(x << " => " << bs.index(x) << ": " << boolalpha << (bs.index(x) == iref)); \
    if (bs.index(x) != iref) rtn = 1
  
  #define TESTINDEXING(bn, i, j, k, global )\
    MSG("i ref" << i << " => i calc " << bn.globalToLocalIndices(global)[0] << ": " << boolalpha << (bn.globalToLocalIndices(global)[0]==i)); \
    MSG("j ref" << j << " => j calc " << bn.globalToLocalIndices(global)[1] << ": " << boolalpha << (bn.globalToLocalIndices(global)[1]==j)); \
    MSG("k ref" << k << " => k calc " << bn.globalToLocalIndices(global)[2] << ": " << boolalpha << (bn.globalToLocalIndices(global)[2]==k)); \
    MSG("global ref" << global << " => global calc " << bn.localToGlobalIndex({i,j,k}) << ": " << boolalpha << (bn.localToGlobalIndex({i,j,k})==global)); \
    if (bn.globalToLocalIndices(global)[0]!=i) rtn = 1; \
    if (bn.globalToLocalIndices(global)[1]!=j) rtn = 1; \
    if (bn.globalToLocalIndices(global)[2]!=k) rtn = 1; \
    if (bn.localToGlobalIndex({i,j,k})!=global) rtn = 1; 
  

//   const vector<double> linedges = linspace(3, 1, 101);
//   cout << "Lin edges: "; for (double x : linedges) cout << x << " "; cout << endl;
//   YODA::BinnedAxis bs1(linedges);
//   TESTBS(bs1, 0, 0);

//   const vector<double> logedges = logspace(3, 1, 101);
//   cout << "Log edges: "; for (double x : logedges) cout << x << " "; cout << endl;
//   YODA::BinnedAxis bs2(logedges);
//   TESTBS(bs2, 0, 0);

//   const vector<double> linedges2 = linspace(3, -1, 101);
//   cout << "Lin edges starting below 0: "; for (double x : logedges) cout << x << " "; cout << endl;
//   YODA::BinnedAxis bs3(linedges2);
//   TESTBS(bs3, -1-1e-5, 0);

//   std::vector< YODA:: BinnedAxis> bs;
//   bs.push_back(bs1);
//   YODA::Binning<1> bn1  (bs);
//   YODA::Binning<1> bn1_alt  (bs);
//   bs.push_back(bs2);
//   YODA::Binning<2> bn2  (bs);
//   bs.push_back(bs3);
//   YODA::Binning<3> bn3  (bs);
  
//   const vector<double> logedges_alt = logspace(3, 10, 101);
//   YODA::BinnedAxis bs2_alt(logedges_alt);

//   std::vector< YODA:: BinnedAxis> bs_alt;
//   bs_alt.push_back(bs1);
//   bs_alt.push_back(bs2_alt);
//   bs_alt.push_back(bs3);
//   YODA::Binning<3> bn3_alt  (bs_alt);

  
  
  
//   int gIndex=0;
//   for (size_t k=0; k<bs3.size()+1 ; k++){
//     std::vector<std::string> lines;
//     for (size_t j=0; j<bs2.size()+1 ; j++){
//       std::string thisLine="";
//       for (size_t i=0; i<bs1.size()+1 ; i++){
//         TESTINDEXING(bn3, int(i), int(j), int(k), gIndex);
//         gIndex++;
//       }
//       lines.push_back(thisLine);
//     }
//   }
  
//   std::cout << " Check if two binnings which are the same are deemed compatible. bn1==bn1_alt? "<< (bn1.isCompatible(bn1_alt)) << std::endl;
//   if (!bn1.isCompatible(bn1_alt)) rtn=1;
//   std::cout << " Check if two binnings which have different edges are deemed compatible. bn3==bn3_alt? "<< (bn3.isCompatible(bn3_alt)) << std::endl;
//   if (bn3.isCompatible(bn3_alt)) rtn=1;

//   return rtn;
// }
