# BinnedDbn, Histograms and Profiles

YODA's main histogramming class is the `BinnedDbn` class,
which is a specialised `FillableStorage` for the bin content
type `Dbn`.

It is templated on the number of fill dimensions
and the axis edge types.
If the number of fill dimensions is the same as
the number of binned axes, the `BinnedDbn` is
usually called a binned histogram.
An example could look like this:

```cpp
BinnedDbn<1,double> bDbn_1D;
BinnedHisto<double> bHisto_1D;
BinnedHisto<double,std::string> bHisto_2D;
```

If the number of fill dimensions is one unit higher
than the number of binned axes, the `BinnedDbn` is
usually called a binned profile:

```cpp
BinnedDbn<2,int> bDbn_1D;
BinnedProfile<int> bProfile_1D;
BinnedProfile<int,double> bProfile_2D;
```

Most of the time, the user would deal with
histograms or profiles that only have
continuous axes. In that case convenient
aliases are defined:

```cpp
Histo1D h_1D; // histogram with 1 continuous axis
Histo2D h_2D; // histogram with 2 continuous axes
Histo3D h_3D; // histogram with 3 continuous axes
HistoND<m> h_ND; // histogram with m continuous axes
Profile1D p_1D; // profile with 1 continuous axis
Profile2D p_2D; // profile with 2 continuous axes
Profile3D p_3D; // profile with 3 continuous axes
ProfileND<m> p_ND; // profile with m continuous axes
```

## Constructors

You can construct a `BinnedDbn`-like class either
via [Binning object](Binning.md),
via [Axis objects](BinnedAxis.md),
via lists of edes
(e.g. brace-initialised lists or vectors),
or - in the case of all-continuous axes -
you can also specify the number of bins within
a continuous range, leaving it to the class
to work out the edges itself.

```cpp
Histo1D h1(10, 0, 100); // 10 bins between 0 and 100
std::vector<double> edges = {0, 10, 20, 30, 40, 50};
Histo1D h2(edges);
BinnedHisto2D<int, std::string> h3({ 1, 2, 3 }, { "A", "B", "C" });
```

## Histogram vs Profile

The main difference is the number of fill dimensions.
In histograms, the dimension of the bin content type,
i.e. the `Dbn` class, is the same as number of binned
axes.
In profiles, the dimension of the bin content type
is one higher than the number of binned axes.

```cpp
Histo1D h1(5, 0.0, 1.0);
h1.fill(0.2);
Profile1D p1(5, 0.0, 1.0);
p1.fill(0.2, 3.5);
```

The additional fill dimension in the profile
is used by the `Dbn` to calculate various
statistical moments.

## Global vs local bin index

In YODA2, histograms are generalised to an (in principle)
arbitrary number of dimensions. Under-/over-/otherflows are
automatically wrapped around the visible bin layout specified
via the edges. This is best illustrated with an example:

```cpp
size_t nbinsX = 4, nbinsY = 6;
double lowerX = 0, lowerY = 0;
double upperX = 4, upperY = 6;
Histo2D h2(nbinsX, lowerX, upperX,
           nbinsY, lowerY, upperY);
std::cout << "H2 has fillDim = " << h2.fillDim()
          << " and binDim = " << h2.binDim() << std::endl;
std::cout << "H2 has " << h2.numBinsX() << " x "
          << h2.numBinsY() << " = "
          << h2.numBins() << " visible bins." << std::endl;
std::cout << "Including masked bins, H2 has ";
std::cout << h2.numBins(true) << " bins."  << std::endl;

double w = 0;
for (auto& b : h2.bins()) {
  h2.fill(b.xMid(), b.yMid(), ++w); // uses global bin index
}
```

```
H2 has fillDim = 2 and binDim = 2
H2 has 4 x 6 = 24 visible bins.
Including masked bins, H2 has 48 bins.
```

This is a 2D histogram with two continuous axes,
where each visible bin is filled with its (visible) bin index.

The method `numBins(true)` yields the total number of bins
including under/overflows from which the global bin index can
be constructed:

```cpp
for (size_t i = 0; i < h2.numBins(true); ++i) {
  assert (i == h2.bin(i).index());
}
```

The visible layout is shown here, being constructed
from the local bin indices:

```cpp
std::cout << "H2 bins using local indices:" << std::endl;
for (size_t idxY = 0; idxY < h2.numBinsY(); ++idxY) {
  for (size_t idxX = 0; idxX < h2.numBinsX(); ++idxX) {
    // indices along any given axis are 0-indexed and include under/overflows
    // offset by 1 to account for this:
    std::cout << "\t(" << idxX+1 << "," << idxY+1 << ") = ";
    std::cout << h2.bin(idxX+1, idxY+1).sumW();
  }
  std::cout << std::endl;
}
std::cout << std::endl;
```

```
H2 bins using local indices:
	(1,1) = 1	(2,1) = 2	(3,1) = 3	(4,1) = 4
	(1,2) = 5	(2,2) = 6	(3,2) = 7	(4,2) = 8
	(1,3) = 9	(2,3) = 10	(3,3) = 11	(4,3) = 12
	(1,4) = 13	(2,4) = 14	(3,4) = 15	(4,4) = 16
	(1,5) = 17	(2,5) = 18	(3,5) = 19	(4,5) = 20
	(1,6) = 21	(2,6) = 22	(3,6) = 23	(4,6) = 24
```

When including under-/overflows, you can see the padding
around the visible bins:

```cpp
std::cout << "H2 bins using local indices + under/overflows:" << std::endl;
for (size_t idxY = 0; idxY < h2.numBinsY(true); ++idxY) {
  for (size_t idxX = 0; idxX < h2.numBinsX(true); ++idxX) {
    std::cout << "\t(" << idxX << "," << idxY << ") = ";
    std::cout << h2.bin(idxX, idxY).sumW();
  }
  std::cout << std::endl;
}
std::cout << std::endl;
```

```
H2 bins using local indices + under/overflows:
	(0,0) = 0	(1,0) = 0	(2,0) = 0	(3,0) = 0	(4,0) = 0	(5,0) = 0
	(0,1) = 0	(1,1) = 1	(2,1) = 2	(3,1) = 3	(4,1) = 4	(5,1) = 0
	(0,2) = 0	(1,2) = 5	(2,2) = 6	(3,2) = 7	(4,2) = 8	(5,2) = 0
	(0,3) = 0	(1,3) = 9	(2,3) = 10	(3,3) = 11	(4,3) = 12	(5,3) = 0
	(0,4) = 0	(1,4) = 13	(2,4) = 14	(3,4) = 15	(4,4) = 16	(5,4) = 0
	(0,5) = 0	(1,5) = 17	(2,5) = 18	(3,5) = 19	(4,5) = 20	(5,5) = 0
	(0,6) = 0	(1,6) = 21	(2,6) = 22	(3,6) = 23	(4,6) = 24	(5,6) = 0
	(0,7) = 0	(1,7) = 0	(2,7) = 0	(3,7) = 0	(4,7) = 0	(5,7) = 0
```

## Masking bins

The `maskBin(size_t)` and `maskBins(vector<size_t>)` methods
can be used to mask individial bins:

```cpp
std::cout << "# bins before: " << h2.numBins(true);
std::cout << "  isMasked? " << h2.isMasked(10) << std::endl;
h2.maskBin(10);
std::cout << "# bins after:  " << h2.numBins(true);
std::cout << "  isMasked? " << h2.isMasked(10) << std::endl;
```
```
# bins before: 48  isMasked? 0
# bins after:  47  isMasked? 1
```

Both the `bins()` and `numBins()` method take an additional Boolean
as argument to recover masked bins:

```cpp
std::cout << "# bins excluding masked bins: ";
std::cout << h2.numBins(true) << std::endl;
std::cout << "# bins including masked bins: ";
std::cout << h2.numBins(true, true) << std::endl;
```
```
# bins excluding masked bins: 47
# bins including masked bins: 48
```

The (global) indices of masked bins
can be retrived like so:

```cpp
for (size_t i : h2.maskedBins()) {
  std::cout << "\t" << i;
}
std::cout << std::endl;
```
```
	10
```

## Rebinning


```cpp
std::cout << "nBins before: " << h2.numBinsX();
std::cout << "  " << h2.numBinsY() << std::endl;
std::cout << "x-edges before:" << std::endl;
for (double edge : h2.xEdges()) {
  std::cout << "\t" << edge;
}
std::cout << std::endl;
std::cout << "y-edges before:" << std::endl;
for (double edge : h2.yEdges()) {
  std::cout << "\t" << edge;
}
std::cout << std::endl;
std::cout << "bin values before:" << std::endl;
for (size_t idxY = 0; idxY < h2.numBinsY(); ++idxY) {
  for (size_t idxX = 0; idxX < h2.numBinsX(); ++idxX) {
    std::cout << "\t" << h2.bin(idxX+1, idxY+1).sumW();
  }
  std::cout << std::endl;
}
std::cout << std::endl;
```

```
nBins before: 4  6
x-edges before:
	0	1	2	3	4
y-edges before:
	0	1	2	3	4	5	6
bin values before:
	1	2	3	4
	5	6	7	8
	9	10	11	12
	13	14	15	16
	17	18	19	20
	21	22	23	24
```

Rebinning both the x-axis and the y-axis
by factors of 2 yields

```cpp
h2.rebinX(2);
h2.rebinY(2);
std::cout << "nBins after: " << h2.numBinsX();
std::cout << "  " << h2.numBinsY() << std::endl;
std::cout << "x-edges after:" << std::endl;
for (double edge : h2.xEdges()) {
  std::cout << "\t" << edge;
}
std::cout << std::endl;
std::cout << "y-edges after:" << std::endl;
for (double edge : h2.yEdges()) {
  std::cout << "\t" << edge;
}
std::cout << std::endl;
std::cout << "bin values after:" << std::endl;
for (size_t idxY = 0; idxY < h2.numBinsY(); ++idxY) {
  for (size_t idxX = 0; idxX < h2.numBinsX(); ++idxX) {
    std::cout << "\t" << h2.bin(idxX+1, idxY+1).sumW();
  }
  std::cout << std::endl;
}
std::cout << std::endl;
```

```
nBins after: 2  3
x-edges after:
	0	2	4
y-edges after:
	0	2	4	6
bin values after:
	14	22
	46	54
	78	86
```

Alternatively, we could specify the new edges
directly, provided they are a subset of
the original set of bin edges of course:

```cpp
Histo1D h1(std::vector<double>{-3, -1, 0, 2, 10, 42});
std::cout << "x-edges before:" << std::endl;
for (double edge : h1.xEdges()) {
  std::cout << "\t" << edge;
}
std::cout << std::endl;
h1.rebinX({-3., 2., 10.});
std::cout << "x-edges after:" << std::endl;
for (double edge : h1.xEdges()) {
  std::cout << "\t" << edge;
}
std::cout << std::endl;
```

```
x-edges before:
	-3	-1	0	2	10	42
x-edges after:
	-3	2	10
```


## Bin edges and bin widths

The continuous axis is like a number axis spanning
the full range from `-inf` to `+inf`, which are also
the outermost edges, when including under/overflow bins:

```cpp
Histo1D h1(std::vector<double>{-3, -1, 0, 2, 10, 42});
std::cout << "H1 edges along x-axis:" << std::endl;
for (auto edge : h1.xEdges(true)) {
  std::cout << "\t" << edge;
}
std::cout << std::endl;
```

```
H1 edges along x-axis:
	-inf	-3	-1	0	2	10	42	inf
```

As a result, the bin widths of the under/overflow
will always be infinite for continuous axes:
```cpp
std::cout << "H1 bin widths along x-axis:" << std::endl;
for (auto width : h1.xWidths(true)) {
  std::cout << "\t" << width;
}
std::cout << std::endl;
```

```
H1 bin widths along x-axis:
	inf	2	1	2	8	32	inf
```

## NaN treatment

If one of the coordinates going into the fill function
happens to be a `NaN`, the fill will be skipped.
Internally, the `BinnedDbn` will keep track of
how many `NaN`s have been encountered during a run
and what the overall weight contribution would have been
to the total sum of weights.

```cpp
const double nanval = std::numeric_limits<double>::quiet_NaN();
Histo1D h1(5, 0.0, 1.0);
h1.fill(1.2); h1.fill(3.4);
std::cout << "# entries: " << h1.numEntries() << std::endl;
h1.fill(nanval, 10);
std::cout << "# entries: " << h1.numEntries() << std::endl;
std::cout << "NaN count: " << h1.nanCount() << std::endl;
std::cout << "NaN sumW:  " << h1.nanSumW()  << std::endl;
std::cout << "NaN sumW2: " << h1.nanSumW2() << std::endl;
```
```
# entries: 2
# entries: 2
NaN count: 1
NaN sumW:  10
NaN sumW2: 100
```

## Type reductions

It's possible to make a histogram from a profile
(i.e. removing the additioal fill dimension):

```cpp
Profile1D p1(5, 0, 1);
Histo1D h1 = p1.mkHisto();
assert(h1 == Histo1D(5, 0, 1));
```

Producing lower-dimensional objects can be achieved via slicing
or marginalising along a specific axis. In case of slicing,
the result will be a vector of histograms or profiles where
the binning dimension of the returned objects is reduced by
one unit (and so the initial object needs to have at least
two binning dimensions). The resulting vector will have one
element for every bin along the axis that was sliced along:

```cpp
BinnedHisto2D<int,std::string> h2D({1,2,3}, {"A", "B", "C"});
std::vector<Histo1D> H1Dslices = h2D.mkHistos<1>(); // along the second axis

BinnedProfile2D<int,std::string> p2D({1,2,3}, {"A", "B", "C"});
std::vector<Profile1D> P1Dslices = p2D.mkProfiles<1>(); // along the second axis
```

When marginalising along an axis, all the bins will be collapsed
into the remaining axes:

```cpp
/// Case 1: BinnedProfile(N+1)D -> BinnedHistoND
/// The binning is reduced by one dimension,
/// and the Dbn is reduced by two dimensions.
BinnedHisto1D<int> h1D_i1 = h2D.mkMarginalHisto<1>();
BinnedHisto1D<std::string> h1D_s1 = h2D.mkMarginalHisto<0>();
std::cout << h2D.fillDim() << " -> " << h1D_i1.fillDim();
std::cout << " , " << h1D_s1.fillDim() << std::endl;

/// Case 2: BinnedHisto(N+1)D -> BinnedHistoND
/// Both the Dbn and the binning are reduced by one dimension.
BinnedHisto1D<int> h1D_i2 = p2D.mkMarginalHisto<1>();
BinnedHisto1D<std::string> h1D_s2 = p2D.mkMarginalHisto<0>();
std::cout << p2D.fillDim() << " -> " << h1D_i2.fillDim();
std::cout << " , " << h1D_s2.fillDim() << std::endl;
```
```
2 -> 1 , 1
3 -> 1 , 1
```

Produce a lower-dimensional profile from a higher-dimensional
histogram or profile:

```cpp
/// Case 1: BinnedHisto(N+1)D -> BinnedProfileND
/// The Dbn dim is the same, but the binning is reduced by one dimension.
BinnedHisto2D<int,std::string> h2D({1,2,3}, {"A", "B", "C"});
BinnedProfile1D<int> p1D_i1 = h2D.mkMarginalProfile<1>();
BinnedProfile1D<std::string> p1D_s1 = h2D.mkMarginalProfile<0>();
std::cout << h2D.fillDim() << " -> " << p1D_i1.fillDim();
std::cout << " , " << p1D_s1.fillDim() << std::endl;

/// Case 2: BinnedProfile(N+1)D -> BinnedProfileND
/// Both the Dbn and the binning are reduced by one dimension.
BinnedProfile2D<int,std::string> p2D({1,2,3}, {"A", "B", "C"});
BinnedProfile1D<int> p1D_i2 = p2D.mkMarginalProfile<1>();
BinnedProfile1D<std::string> p1D_s2 = p2D.mkMarginalProfile<0>();
std::cout << p2D.fillDim() << " -> " << p1D_i2.fillDim();
std::cout << " , " << p1D_s2.fillDim() << std::endl;
```
```
2 -> 2 , 2
3 -> 2 , 2
```

Histograms and profiles can be turned into a `BinnedEstimate`
like so:

```cpp
Estimate1D e1 = h1.mkEstimate();
Estimate1D e2 = p1.mkEstimate();
assert(e1 == e2);
```

Note that by default the value of the `Estimate`
corresponds to the density estimated by the `Dbn`,
i.e. the sum of weights will be divided by the
bin width. The `mkEstimate()` method accepts
an optional Boolean to prevent this.

If the division by bin volume is desired, note that
the under- and overflow bins are by default skipped,
since the bin width is technically infinite.
In order to avoid the infinity-division when
propgating the values from the under-/overflows,
a custom "overflow bin width" can be provided:
```cpp
Estimate1D e3 = h1.mkEstimate(/*path=*/"",
                              /*source=*/"",
                              /*divbyvol=*/true,
                              /*overflowWidth=*/1.0);
```

For higher-dimensional objects it might be preferable to
slice the resulting `BinnedEstimate` along one axis into
lower-dimensional versions, e.g. to plot a 2D distribution
as series of 1D distributions:

```cpp
BinnedHisto2D<int,std::string> h2({1,2,3}, {"A", "B", "C"});
BinnedEstimate2D<int,std::string> e2D = h2.mkEstimate();
std::vector<BinnedEstimate1D<std::string>> e1D_s = h2.mkEstimates<0>(); // slice along int axis
std::vector<BinnedEstimate1D<int>> e1D_i = h2.mkEstimates<1>(); // slice along string axis
std::cout << e2D.type() << std::endl;
for (const auto& ao : e1D_s) {
  std::cout << "\t" << ao.type();
}
std::cout << std::endl;
for (const auto& ao : e1D_i) {
  std::cout << "\t" << ao.type();
}
std::cout << std::endl;
```
```
BinnedEstimate<i,s>
	BinnedEstimate<s>	BinnedEstimate<s>	BinnedEstimate<s>
	BinnedEstimate<i>	BinnedEstimate<i>	BinnedEstimate<i>
```

It's also possible to create a `Scatter` objects directly
from histograms and profiles:

```cpp
Scatter2D s1 = h1.mkScatter();
Scatter2D s2 = p1.mkScatter();
```

For the `BinnedDbn` class, the `mkScatter()` method
also accepts an optional Boolean that sets
the resulting point value along a binned axis to
the bin focus rather than the centre of the bin.
