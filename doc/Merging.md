<h1> Merging and Stacking </h1>

[ToC]

## Merging

To merge multiple YODA files, use:
```
  yodamerge -o combined.yoda.gz file1.yoda.gz file2.yoda.gz ...
```

This creates `combined.yoda.gz`, where inert objects are statistically
combined by unscaling, summing, and reapplying scaling.

### Merging inert objects


By default, inert objects are averaged, assuming equal event counts in the input files.
This can be changed using `--s2d-mode` (or `--s1d-mode`, `--s3d-mode` for 1D/3D scatters):
* Average values (default)
  ```
  yodamerge file1.yoda.gz file2.yoda.gz -o merged.yoda.gz --s2d-mode assume_mean
  ```
*	Sum scatter points instead of averaging
  ```
  yodamerge file1.yoda.gz file2.yoda.gz -o merged.yoda.gz --s2d-mode add
  ```
* Concatenate points instead of merging
  ```
  yodamerge file1.yoda.gz file2.yoda.gz -o merged.yoda.gz --s2d-mode combine
  ```
* Keep only the first encountered scatter object
  ```
  yodamerge file1.yoda.gz file2.yoda.gz -o merged.yoda.gz --s2d-mode first
  ```

### Type mismatches

If objects with the same name but different types exist, choose how to handle them:
```
--type-mismatch-mode first   # Keep the first encountered object
--type-mismatch-mode scatter # Convert all to Scatter objects
```


## Stacking

To stack histograms without rescaling, use:
```
  yodastack -o stacked.yoda.gz file1.yoda.gz file2.yoda.gz ...
```

This simply adds bin contents without adjusting for different run sizes.

To apply custom scaling to input files:
```
  yodastack -o stacked.yoda.gz file1.yoda.gz:1.23 file2.yoda.gz:4.56
```
Here, each file is weighted by the specified factor.


## Custom Merging

For more control, you can modify yodamerge or use the YODA Python API to implement your own merging rules.


## Outlier detection

When merging multiple files from statistically independent runs, sum runs may contain
statistical outliers that you might want to exclude. To identify files that could
introduce anomalies or spikes in the merged distributions, you can use the Python API
to perform a $\chi^2$ test, for example, by comparing the sum of weights:
```python
import sys, math, yoda

if len(sys.argv) < 2:
    print("Provide input files as script arguments!")
    sys.exit(1)

match = "^/_EVTCOUNT$" # or "/_XS", ...
level = 3.0 # sigma

sumw = [ yoda.read(fname, asdict=False, patterns=match)[0].val() for fname in sys.argv[1:] ]
mean = sum(sumw)/float(len(sumw))
var  = sum([ v ** 2 for v in sumw])/float(len(sumw)) - mean ** 2
stddev = math.sqrt(var) if var > 0 else 0.

for fname in sys.argv[1:]:
    if abs(yoda.read(fname, asdict=False, patterns=match)[0].val() - mean) > level*stddev:
        print('Potential outlier detected at the %.1f sigma level: %s' % (level, fname))
```
