# Points

YODA's `PointND` class is a set of `N` values with
associated errors. The errors are unsigned,
i.e. they merely reflect the size of the error bar
in a scatter plot.

```cpp
Point1D p1(1.0, 0.1);
Point1D p2(1.0, 0.1, 0.2);
Point1D p3(1.0, {0.3, 0.4});
Point2D p4(1.0, 2.0, {0.3, 0.4}, {0.5, 0.6});
std::cout << p1.val() << " - " << p1.errs()[0].first;
std::cout << " + " << p1.errs()[0].second << std::endl;
std::cout << p2.val() << " - " << p2.errs()[0].first;
std::cout << " + " << p2.errs()[0].second << std::endl;
std::cout << p3.val() << " - " << p3.errs()[0].first;
std::cout << " + " << p3.errs()[0].second << std::endl;
std::cout << p4.val(0) << " - " << p4.errs()[0].first;
std::cout << " + " << p4.errs()[0].second << "  and  ";
std::cout << p4.val(1) << " - " << p4.errs()[1].first;
std::cout << " + " << p4.errs()[1].second << std::endl;
```
```
1 - 0.1 + 0.1
1 - 0.1 + 0.2
1 - 0.3 + 0.4
1 - 0.3 + 0.4  and  2 - 0.5 + 0.6
```
